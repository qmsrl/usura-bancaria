<?php

use Behat\Behat\Context\Context;
use Behat\Behat\Context\SnippetAcceptingContext;
use Behat\MinkExtension\Context\MinkContext;

/**
 * Defines application features from the specific context.
 */
class FeatureContext extends MinkContext implements Context,SnippetAcceptingContext
{
    /**
     * Initializes context.
     *
     * Every scenario gets its own context instance.
     * You can also pass arbitrary arguments to the
     * context constructor through behat.yml.
     */
    public function __construct()
    {
    }

    /**
     * Click on the element with the provided xpath query.
     *
     * @When I click on the element :css
     */
    public function iClickOnTheElement($css)
    {
        $element = $this->findElementInPageByCss($css);

        // errors must not pass silently
        if (null === $element) {
            throw new \InvalidArgumentException(sprintf('Could not evaluate css: "%s"', $css));
        }

        // ok, let's click on it
        $element->click();
    }

    /**
     * @param $time integer
     * @Given I wait a while
     * @Given I wait :time
     */
    public function iWait($time = 3000)
    {
        $this->getSession()->wait($time);
    }

    /**
     * @param $time integer
     * @param $element string
     * @Then I wait until I do not see :element
     * @Then I wait until I do not see :element :time
     *
     * @return bool
     */
    public function iWaitUntilIDoNotsee($element, $time = 2000)
    {
        $this->getSession()->wait($time);
        for ($i = 0; $i == 5; ++$i) {
            $actual = $this->getSession()->getPage()->getText();
            $actual = preg_replace('/\s+/u', ' ', $actual);
            $regex = '/' . preg_quote($element, '/') . '/ui';

            if (preg_match($regex, $actual)) {
                break;
            }
            $this->getSession()->wait($time);
        }

        $this->assertPageNotContainsText($element);
    }

    /**
     * @param $time integer
     * @param $element string
     * @Then I wait until I see :element
     * @Then I wait until I see :element :time
     *
     * @return bool
     */
    public function iWaitUntilIsee($element, $time = 2000)
    {
        $this->getSession()->wait($time);
        for ($i = 0; $i == 5; ++$i) {
            $actual = $this->getSession()->getPage()->getText();
            $actual = preg_replace('/\s+/u', ' ', $actual);
            $regex = '/' . preg_quote($element, '/') . '/ui';

            if (preg_match($regex, $actual)) {
                break;
            }
            $this->getSession()->wait($time);
        }

        $this->assertPageContainsText($element);
    }

    /**
     * @param $time integer
     * @param $text string
     * @param $element string
     * @Then I wait until I see :text in the :css element
     * @Then I wait :time until I see :text in the :css element
     *
     * @return bool
     */
    public function iWaitUntilIseeTextInTheElement($element, $text, $time = 2000)
    {
        $this->getSession()->wait($time);
        for ($i = 0; $i == 5; ++$i) {
            $actual = $this->getSession()->getPage()->getText();
            $actual = preg_replace('/\s+/u', ' ', $actual);
            $regex = '/' . preg_quote($text, '/') . '/ui';

            if (preg_match($regex, $actual)) {
                break;
            }
            $this->getSession()->wait($time);
        }

        $this->assertElementContainsText($element, $text);
    }

    /**
     * @param $time integer
     * @param $text string
     * @param $element string
     * @Then I wait until I do not see :text in the :css element
     * @Then I wait :time until I do not see :text in the :css element
     *
     * @return bool
     */
    public function iWaitUntilIDoNotseeTextInTheElement($element, $text, $time = 2000)
    {
        $this->getSession()->wait($time);
        for ($i = 0; $i == 5; ++$i) {
            $actual = $this->getSession()->getPage()->getText();
            $actual = preg_replace('/\s+/u', ' ', $actual);
            $regex = '/' . preg_quote($text, '/') . '/ui';

            if (preg_match($regex, $actual)) {
                break;
            }
            $this->getSession()->wait($time);
        }

        $this->assertElementNotContainsText($element, $text);
    }

    /**
     * @Then I should see the :css element visible
     *
     */
    public function iWaitUntilIseeFormElement($element)
    {
        $page = $this->getSession()->getPage();

        $this->assertElementOnPage($element);

        if (!$page->find('css', $element)->isVisible()) {
            throw new Exception("Element $element is not visible on the page");
        }

    }

    /**
     * @Then I should see the :css element not visible
     *
     */
    public function iWaitUntilINotseeFormElement($element)
    {
        $page = $this->getSession()->getPage();

        $this->assertElementOnPage($element);

        if ($page->find('css', $element)->isVisible()) {
            throw new Exception("Element $element is visible on the page");
        }
    }
}
