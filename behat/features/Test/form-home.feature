Feature: Compilo il modulo sulla home
  Navigando sul sito
  compilo una richesta

  @javascript
  Scenario: compilo il form sulla home page come impresa
    Given I go to "/"
    Then I should see "Calcola quanto puoi recuperare dalla Tua Banca!"
    # Clicco sul pulsante AVANTI
    When I press "bottone"
    Then I wait 5000
    And I should see "Seleziona una di queste opzioni:" in the "#tipo" element
    #scelgo per persona
    When I select "persona" from "contatto_tipo_0"
    Then I should see the "#contatto_nome" element visible
    And I should see the "#contatto_cognome" element visible
    And I should see the "#contatto_ragioneSociale" element not visible
    And I should see the "#contatto_nominativoReferente" element not visible
    # Clicco sul pulsante AVANTI
    When I press "bottone"
    Then I wait 5000
    And I should see "Compila questo campo:" in the "#recapitiTelefonici" element
    And I should see "Compila questo campo:" in the "#email" element
    # Compilo i campi obbligatori ma sbaglio l'email
    And I fill in "contatto_recapitiTelefonici" with "123"
    And I fill in "contatto_email" with "123"
    And I press "bottone"
    And I wait 5000
    And I should see "Inserisci un indirizzo email valido:" in the "#email" element
    # Correggo l'email
    And I fill in "contatto_email" with "test@test.it"
    # Passo allo step 2
    When I press "bottone"
    Then I wait 5000
    And I should see "Step 2 - Indica i tuoi rapporti bancari." in the "#titoloStepSecondo" element
    # Torno indietro
    When I press "bottoneIndietro"
    Then I wait 5000
    And I should see "Calcola Subito" in the "#titoloStepPrimo" element
    # Controllo che i campi siano compilati
    And the "contatto_email" field should contain "test@test.it"
    And the "contatto_recapitiTelefonici" field should contain "123"
    # Cambio il primo select
    When I select "aziendaImpresa" from "contatto_tipo_1"
    Then I should see the "#contatto_nome" element not visible
    And I should see the "#contatto_cognome" element not visible
    And I should see the "#contatto_ragioneSociale" element visible
    And I should see the "#contatto_nominativoReferente" element visible
    And I fill in "contatto_ragioneSociale" with "HOME"
    # Passo allo step 2
    When I press "bottone"
    Then I wait 5000
    And I should see "Step 2 - Indica i tuoi rapporti bancari." in the "#titoloStepSecondo" element
    And I should see the "#numeroConti" element visible
    And I should see the "#numeroConti" element visible
    And I should see the "#contatto_lineaFido" element visible
    And I should see the "#contatto_importo" element not visible
    And I should see the "#contatto_lineaCredito" element visible
    And the "contatto_privacy" checkbox should be checked
    # Proseguo senza compilare i campi obbligatori
    When I press "bottone"
    Then I wait 5000
    And I should see "Seleziona una di queste opzioni:" in the "#lineaFido" element
    And I should see "Compila questo campo:" in the "#numeroConti" element
    # Compilo lineaFido si
    When I select "si" from "contatto_lineaFido_0"
    Then I should not see "Seleziona una di queste opzioni:" in the "#lineaFido" element
    And I should see "Compila questo campo:" in the "#numeroConti" element
    And I should see the "#contatto_importo" element visible
    # Compilo LineaFido no
    When I select "no" from "contatto_lineaFido_0"
    Then I should not see "Seleziona una di queste opzioni:" in the "#lineaFido" element
    And I should see "Compila questo campo:" in the "#numeroConti" element
    And I should see the "#contatto_importo" element not visible
    # compilo contatto_numeroConti
    When I fill in "contatto_numeroConti" with "123"
    Then I should not see "Compila questo campo:" in the "#numeroConti" element
    # concludo il form
    When I press "bottone"
    Then I wait 10000
    And I should see "Richiesta inviata con successo"
    And I should see "Grazie per averci scritto, a breve sarai contattato da un nostro esperto!"

  @javascript
  Scenario: compilo il form sulla home page come persona
    Given I go to "/"
    Then I should see "Calcola quanto puoi recuperare dalla Tua Banca!"
  #scelgo per persona
    When I select "persona" from "contatto_tipo_0"
    Then I should see the "#contatto_nome" element visible
    And I should see the "#contatto_cognome" element visible
    And I should see the "#contatto_ragioneSociale" element not visible
    And I should see the "#contatto_nominativoReferente" element not visible
  # Compilo i campi obbligatori
    And I fill in "contatto_recapitiTelefonici" with "123"
    And I fill in "contatto_email" with "test@test.it"
  # Passo allo step 2
    When I press "bottone"
    Then I wait 5000
    And I should see "Step 2 - Indica i tuoi rapporti bancari." in the "#titoloStepSecondo" element
    And I should see the "#numeroConti" element visible
    And I should see the "#numeroConti" element visible
    And I should see the "#contatto_lineaFido" element visible
    And I should see the "#contatto_importo" element not visible
    And I should see the "#contatto_lineaCredito" element visible
    And the "contatto_privacy" checkbox should be checked
  # Compilo LineaFido no
    When I select "no" from "contatto_lineaFido_0"
  # compilo contatto_numeroConti
    When I fill in "contatto_numeroConti" with "123"
    Then I should not see "Compila questo campo:" in the "#numeroConti" element
  # concludo il form
    When I press "bottone"
    Then I wait 10000
    And I should see "Richiesta inviata con successo"
    And I should see "Grazie per averci scritto, a breve sarai contattato da un nostro esperto!"






