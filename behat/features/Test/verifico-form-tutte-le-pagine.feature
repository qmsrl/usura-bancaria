Feature: Verifico la presenza del form
  Navigando sul sito
  verifico che il form sia visibile su tutte le pagina

  @javascript
  Scenario: calcola subito
    Given I go to "/calcola-subito"
    Then I should see "Compila il form per cominciare il calcolo delle anomalie bancarie." in the "#titoloStepPrimo" element
    And I should see "AVANTI" in the "#bottone" element

  @javascript
  Scenario: analisi conto
    Given I go to "/analisi-conto-corrente"
    Then I should see "Compila il form per cominciare il calcolo delle anomalie bancarie." in the "#titoloStepPrimo" element
    And I should see "AVANTI" in the "#bottone" element

  @javascript
  Scenario: analisi mutuo
    Given I go to "/analisi-mutuo"
    Then I should see "Compila il form per cominciare il calcolo delle anomalie bancarie." in the "#titoloStepPrimo" element
    And I should see "AVANTI" in the "#bottone" element

  @javascript
  Scenario: rottamazione equitalia
    Given I go to "/rottamazione-cartelle-equitalia"
    Then I should see "Compila il form per cominciare il calcolo delle anomalie bancarie." in the "#titoloStepPrimo" element
    And I should see "AVANTI" in the "#bottone" element

  @javascript
  Scenario: analisi leasing
    Given I go to "/analisi-leasing"
    Then I should see "Compila il form per cominciare il calcolo delle anomalie bancarie." in the "#titoloStepPrimo" element
    And I should see "AVANTI" in the "#bottone" element

  @javascript
  Scenario: conto anticipi
    Given I go to "/analisi-conto-anticipi"
    Then I should see "Compila il form per cominciare il calcolo delle anomalie bancarie." in the "#titoloStepPrimo" element
    And I should see "AVANTI" in the "#bottone" element

  @javascript
  Scenario: analisi finanziamenti
    Given I go to "/analisi-finanziamenti"
    Then I should see "Compila il form per cominciare il calcolo delle anomalie bancarie." in the "#titoloStepPrimo" element
    And I should see "AVANTI" in the "#bottone" element