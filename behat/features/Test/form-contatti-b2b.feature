Feature: Compilo il modulo sulla pagina clienti b2b e contatti
  Navigando sul sito
  compilo una richesta di contatto

  @javascript
  @check1
  Scenario: compilo il form sulla pagina b2b
    Given I go to "/clienti-business"
    Then I should see "Clienti Business"
    # Clicco sul pulsante AVANTI
    When I press "bottone"
    Then I wait 5000
    And I should see "Compila questo campo:"
    # Compilo i campi obbligatori ma sbaglio l'email
    And I fill in "form_contatti_nome" with "mario"
    And I fill in "form_contatti_cognome" with "rossi"
    And I fill in "form_contatti_email" with "123"
    And I press "bottone"
    And I wait 5000
    And I should see "Inserisci un indirizzo email valido:"
    # Correggo l'email
    And I fill in "form_contatti_email" with "test@test.it"
    And the "form_contatti_privacy" checkbox should be checked
    # Invio il contatto
    When I press "bottone"
    Then I wait 5000
    And I should see "Messaggio inviato con successo"
    And I should see "Grazie per averci scritto, a breve riceverai una nostra risposta"

  @javascript
  @check
  Scenario: compilo il form sulla pagina contatti
    Given I go to "/contatti"
    Then I should see "Contatti"
    And I fill in "form_contatti_messaggio" with "123"
    # Clicco sul pulsante AVANTI
    When I press "bottone"
    Then I wait 5000
    When I press "bottone"
    Then I wait 5000
    And I should see "Compila questo campo:"
    # Compilo i campi obbligatori ma sbaglio l'email
    And I fill in "form_contatti_nome" with "mario"
    And I fill in "form_contatti_cognome" with "rossi"
    And I fill in "form_contatti_email" with "123"
    And I press "bottone"
    And I wait 5000
    And I should see "Inserisci un indirizzo email valido:"
    # Correggo l'email
    And I fill in "form_contatti_email" with "test@test.it"
    And the "form_contatti_privacy" checkbox should be checked
    # Invio il contatto
    When I press "bottone"
    Then I wait 5000
    And I should see "Messaggio inviato con successo"
    And I should see "Grazie per averci scritto, a breve riceverai una nostra risposta"