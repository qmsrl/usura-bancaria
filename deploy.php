<?php
namespace Deployer;
require 'recipe/symfony3.php';


set('repository', 'git@bitbucket.org:qmsrl/usura-bancaria.git');

add('shared_files', ['app/config/parameters.yml', 'web/robots.txt']);
add('shared_dirs', ['var/logs']);

add('writable_dirs', ['var/cache', 'var/logs', 'var/sessions', 'web/assets', 'web/bundles','node_modules']);

// Servers
serverList('servers.yml');
/*server('production', 'calcolousurabancaria.com')
    ->user('username')
    ->identityFile()
    ->set('deploy_path', '/var/www/domain.com');*/


// Tasks

desc('Restart PHP-FPM service');
task('php-fpm:restart', function () {
    // The user must have rights for restart service
    // /etc/sudoers: username ALL=NOPASSWD:/bin/systemctl restart php-fpm.service
    run('sudo systemctl restart php7.0-fpm.service');
});
task('finalize', function () {
    run('sudo /home/ubuntu/finalize.sh {{release_path}}');
});
after('deploy:symlink', 'finalize');
after('deploy:symlink', 'php-fpm:restart');

// Migrate database before symlink new release.

/*before('deploy:symlink', 'database:migrate');*/
/*$username = 'gesto';
$password = 'QMg3st0';
$deployBaseDir = '/home/ubuntu/usura-bancaria';

$flag = false;

if(in_array('stage', $argv)) {
    $flag = true;
}

serverList('servers.yml');

set('ssh_type', 'ssh-rsa');

set('repository', 'git@bitbucket.org:senadaniele/usura-bancaria.git');

// Symfony shared dirs
set('shared_dirs', ['var/logs']);

// Symfony shared files
set('shared_files', ['app/config/parameters.yml', 'web/robots.txt']);

// Symfony writable dirs
set('writable_dirs', ['var/cache', 'var/logs', 'var/sessions', 'web/assets', 'web/bundles']);

// Assets
set('assets', ['web/assets']);


/*
if($flag) {
    task('deploy:clear_controllers', function () {
        run("rm -f {{release_path}}/web/config.php");
    });

    //before('deploy', 'deploy:clear_controllers');
}*/

