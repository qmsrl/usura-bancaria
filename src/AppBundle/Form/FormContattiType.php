<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;

class FormContattiType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nome', TextType::class, array
            (
                'required' => true,
                'attr' => ['placeholder' => 'Nome', 'class' => 'form-control']
            ))
            ->add('cognome', TextType::class, array
            (
                'required' => true,
                'attr' => ['placeholder' => 'Cognome', 'class' => 'form-control']
            ))
            ->add('email', TextType::class, array
            (
                'required' => true,
                'attr' => ['placeholder' => 'Email', 'class' => 'form-control']
            ))
            ->add('RecapitiTelefonici', TextType::class, array
            (
                'required' => true,
                'attr' => ['placeholder' => 'Telefono', 'class' => 'form-control']
            ))
            ->add('messaggio', TextareaType::class, array
            (
                'required' => true,
                'attr' => ['placeholder' => 'Messaggio', 'class' => 'form-control']
            ))
            ->add('privacy', CheckboxType::class, array
            (
                'label' => 'Normativa sulla privacy',
                'required' => true,
            ))
            ->add('pagina', HiddenType::class, array
            (
                'data' => $options['pagina'],
            ));

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Document\Contatto',
            'pagina' => null
        ));
    }

    public function getName()
    {
        return 'form_contatti';
    }
}