<?php
/**
 * Created by PhpStorm.
 * User: daniele
 * Date: 18/11/16
 * Time: 11:15
 */

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;


class ContattoType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('tipo', ChoiceType::class, array
            (
                'label' => 'Sei una:',
                'choices' => [
                    'Persona' => 'persona',
                    'Azienda/Impresa' => 'aziendaImpresa'
                ],
                'required' => true,
                'multiple' => false,
                'expanded' => true,
                'attr' => ['class' => 'form-control ']
            ))
            /*->add('azienda', RadioType::class, array
            (
                'label' => 'Azienda/Impresa',
                'required' => true,
                'attr' => ['class' => 'form-control']
            ))*/
            ->add('nome', TextType::class, array
            (
                'required' => false,
                'attr' => ['placeholder' => 'Nome', 'class' => 'form-control']
            ))
            ->add('cognome', TextType::class, array
            (
                'required' => false,
                'attr' => ['placeholder' => 'Cognome', 'class' => 'form-control']
            ))
            ->add('email', TextType::class, array
            (
                'required' => false,
                'attr' => ['placeholder' => 'Email', 'class' => 'form-control ']
            ))
            ->add('ragioneSociale', TextType::class, array
            (
                'required' => false,
                'attr' => ['placeholder' => 'Ragione sociale', 'class' => 'form-control']
            ))
            ->add('nominativoReferente', TextType::class, array
            (
                'required' => false,
                'mapped' => false,
                'attr' => ['placeholder' => 'Nominativo referente', 'class' => 'form-control']
            ))
            ->add('recapitiTelefonici', TextType::class, array
            (
                'required' => false,
                'attr' => ['placeholder' => 'Numero di telefono', 'class' => 'form-control ']
            ))
            ->add('numeroConti', TextAreaType::class, array
            (
                'required' => false,
                'attr' => ['placeholder' => 'Indica quanti conti correnti e da quanti anni sei correntista', 'class' => 'form-control ']
            ))
            ->add('lineaFido', ChoiceType::class, array
            (
                'label' => 'Ci sono delle linee di fido su alcuni di questi conti correnti?',
                'choices' => [
                    'Sì' => 'si',
                    'No' => 'no'
                ],
                'multiple' => false,
                'expanded' => true,
                'required' => true,
                'attr' => ['class' => 'form-control ']
            ))
            ->add('importo', TextType::class, array
            (
                'required' => false,
                'attr' => ['placeholder' => 'Indica l\'importo globale di affidamento: €', 'class' => 'form-control']
            ))
            ->add('lineaCredito', TextAreaType::class, array
            (
                'required' => false,
                'attr' => ['placeholder' => 'Ci sono altre linee di credito? Esempio: Mutui, Leasing, Prestiti, etc.', 'class' => 'form-control']
            ))
            ->add('privacy', CheckboxType::class, array
            (
                'label' => 'Normativa sulla privacy',
                'required' => false
            ))
            ->add('pagina', HiddenType::class, array
            (
                'data' => $options['pagina'],
            ));




    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Document\Contatto',
            'pagina' => null
        ));
    }

    public function getName()
    {
        return 'contatto';
    }
}