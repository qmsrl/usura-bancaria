<?php
/**
 * Created by PhpStorm.
 * User: daniele
 * Date: 13/12/16
 * Time: 14:58
 */

namespace AppBundle\Controller;


class PageController extends DefaultController
{
    public function chiSiamoAction()
    {
        return $this->render(':default:chiSiamo.html.twig');
    }

    public function privacyAction()
    {
        return $this->render(':default:privacy.html.twig');
    }

    public function condizioniGeneraliAction()
    {
        return $this->render(':default:condizioni-generali.html.twig');
    }

    public function comeFunzionaAction()
    {
        return $this->render(':default:come-funziona.html.twig');
    }

    public function usuraBancariaAction()
    {
        return $this->render(':default:usura-bancaria.html.twig');
    }

    public function anatocismoBancarioAction()
    {
        return $this->render(':default:anatocismo-bancario.html.twig');
    }

    public function mappaDelSitoAction()
    {
        return $this->render(':default:mappa-del-sito.html.twig');
    }
}