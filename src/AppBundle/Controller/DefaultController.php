<?php

namespace AppBundle\Controller;

use AppBundle\Form\ContattoType;
use AppBundle\Form\FormContattiType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Document\Contatto;

class DefaultController extends Controller
{
    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request)
    {
        $contatto = new Contatto();
        $form = $this->createForm(ContattoType::class , $contatto, array(
            'action' => $this->generateUrl('qm_usurabancaria_create'),
            'pagina' => $request->getPathInfo()
          //  'method' => 'PUT'
        ));

        return $this->render('default/index.html.twig', array(
            'form' => $form->createView()
        ));
    }

    public function createAction(Request $request)
    {
        $contatto = new Contatto();
        $form = $this->createForm(ContattoType::class , $contatto);

        $form->handleRequest($request);

        if ($form->isValid()) {
            $contatto->setUserAgent($request->headers->get('User-Agent'));
            $contatto->setIp($request->getClientIp());
            $emailCliente = $contatto->getEmail();
            $email = 'comunicazioni@calcolousurabancaria.it';

            $message = \Swift_Message::newInstance()
                ->setSubject('Calcolo Usura Bancaria')
                ->setFrom('comunicazioni@calcolousurabancaria.it')
                ->setTo($email)
                ->setBody($contatto->getBody())
            ;
            $messageCliente = \Swift_Message::newInstance()
                ->setSubject('Calcolo Usura Bancaria')
                ->setFrom('comunicazioni@calcolousurabancaria.it')
                ->setTo($emailCliente)
                ->setBody($contatto->getBodyCliente())
            ;
            $mailer = $this->get('mailer');
            $mailer->send($message, $messageCliente);

            return $this->redirect($this->generateUrl('qm_usurabancaria_richiesta_success'));
        }

        return $this->render('errore.html.twig',array(
            'form' => $form->createView()
        ));
    }

    public function contattiSuccessAction()
    {
        return $this->render('contattiSuccess.html.twig');
    }

    public function richiestaSuccessAction()
    {
        return $this->render('richiestaSuccess.html.twig');
    }

    public function contattiAction(Request $request)
    {
        $contatto = new Contatto();
        $form = $this->createForm(FormContattiType::class , $contatto , array(
            'action' => $this->generateUrl('qm_usurabancaria_mailContatti'),
            'pagina' => $request->getPathInfo()
            //  'method' => 'PUT'
        ));

        return $this->render('default/contatti.html.twig', array(
            'form' => $form->createView()
        ));
    }

    public function mailContattiAction(Request $request)
    {
        $contatto = new Contatto();
        $form = $this->createForm(FormContattiType::class , $contatto);

        $form->handleRequest($request);

        if ($form->isValid()) {
            $contatto->setUserAgent($request->headers->get('User-Agent'));
            $contatto->setIp($request->getClientIp());
            $contatto->getEmail();
            $email = 'comunicazioni@calcolousurabancaria.it';

            $message = \Swift_Message::newInstance()
                ->setSubject('Calcolo Usura Bancaria')
                ->setFrom($email)
                ->setTo($email)
                ->setBody($contatto->getBodyContatto())
            ;
            $mailer = $this->get('mailer');
            $mailer->send($message);

            return $this->redirect($this->generateUrl('qm_usurabancaria_contatti_success'));
        }

        return $this->render('errore.html.twig',array(
            'form' => $form->createView()
        ));
    }

    public function clientiBusinessAction(Request $request)
    {
        $contatto = new Contatto();
        $form = $this->createForm(FormContattiType::class , $contatto, array(
            'action' => $this->generateUrl('qm_usurabancaria_mailContatti'),
            'pagina' => $request->getPathInfo()
            //  'method' => 'PUT'
        ));

        return $this->render(':default:clienti-business.html.twig', array(
            'form' => $form->createView()
        ));
    }

    public function calcolaSubitoAction(Request $request)
    {
        $contatto = new Contatto();
        $form = $this->createForm(ContattoType::class , $contatto, array(
            'action' => $this->generateUrl('qm_usurabancaria_create'),
            'pagina' => $request->getPathInfo()
            //  'method' => 'PUT'
        ));

        return $this->render(':default:calcola-subito.html.twig', array(
            'form' => $form->createView()
        ));
    }


    public function analisiContoCorrenteAction(Request $request)
    {
        $contatto = new Contatto();
        $form = $this->createForm(ContattoType::class , $contatto, array(
            'action' => $this->generateUrl('qm_usurabancaria_create'),
            'pagina' => $request->getPathInfo()
            //  'method' => 'PUT'
        ));

        return $this->render(':default:analisi-conto-corrente.html.twig', array(
            'form' => $form->createView()
        ));
    }

    public function analisiMutuoAction(Request $request)
    {
        $contatto = new Contatto();
        $form = $this->createForm(ContattoType::class , $contatto, array(
            'action' => $this->generateUrl('qm_usurabancaria_create'),
            'pagina' => $request->getPathInfo()
            //  'method' => 'PUT'
        ));
        return $this->render(':default:analisi-mutuo.html.twig', array(
            'form' => $form->createView()
        ));
    }

    public function rottamazioneCartelleEquitaliaAction(Request $request)
    {
        $contatto = new Contatto();
        $form = $this->createForm(FormContattiType::class , $contatto, array(
            'action' => $this->generateUrl('qm_usurabancaria_mailContatti'),
            'pagina' => $request->getPathInfo()
            //  'method' => 'PUT'
        ));
        return $this->render(':default:rottamazione-cartelle-equitalia.html.twig', array(
            'form' => $form->createView()
        ));
    }

    public function analisiContoAnticipiAction(Request $request)
    {
        $contatto = new Contatto();
        $form = $this->createForm(ContattoType::class , $contatto, array(
            'action' => $this->generateUrl('qm_usurabancaria_create'),
            'pagina' => $request->getPathInfo()
            //  'method' => 'PUT'
        ));
        return $this->render(':default:analisi-conto-anticipi.html.twig', array(
            'form' => $form->createView()
        ));
    }

    public function analisiLeasingAction(Request $request)
    {
        $contatto = new Contatto();
        $form = $this->createForm(ContattoType::class , $contatto, array(
            'action' => $this->generateUrl('qm_usurabancaria_create'),
            'pagina' => $request->getPathInfo()
            //  'method' => 'PUT'
        ));
        return $this->render(':default:analisi-leasing.html.twig', array(
            'form' => $form->createView()
        ));
    }

    public function analisiFinanziamentiAction(Request $request)
    {
        $contatto = new Contatto();
        $form = $this->createForm(ContattoType::class , $contatto, array(
            'action' => $this->generateUrl('qm_usurabancaria_create'),
            'pagina' => $request->getPathInfo()
            //  'method' => 'PUT'
        ));
        return $this->render(':default:analisi-finanziamenti.html.twig', array(
            'form' => $form->createView()
        ));
    }

    public function erroreAction(Request $request)
    {
        $contatto = new Contatto();
        $form = $this->createForm(FormContattiType::class , $contatto, array(
            'action' => $this->generateUrl('qm_usurabancaria_mailContatti'),
            'pagina' => $request->getPathInfo()
            //  'method' => 'PUT'
        ));
        return $this->render('errore.html.twig', array(
            'form' => $form->createView()
        ));
    }
}
