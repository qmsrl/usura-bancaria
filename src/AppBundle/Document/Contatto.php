<?php
/**
 * Created by PhpStorm.
 * User: daniele
 * Date: 18/11/16
 * Time: 11:19
 */

namespace AppBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
use Symfony\Component\Validator\Constraints as Assert;

class Contatto
{
    /**
     * @MongoDB\Id(strategy="INCREMENT")
     */
    protected $id;

    /**
     * @MongoDB\Field(type="string")
     */
    protected $nome;

    /**
     * @MongoDB\Field(type="string")
     */
    protected $cognome;

    /**
     * @MongoDB\Field(type="string")
     */
    protected $settore;

    /**
     * @MongoDB\Field(type="string")
     */
    protected $ragioneSociale;

    /**
     * @MongoDB\NotSaved
     */
    protected $tipo;

    /**
     * @MongoDB\NotSaved
     */
    protected $nominativoReferente;

    /**
     * @MongoDB\NotSaved
     */
    protected $numeroConti;

    /**
     * @MongoDB\NotSaved
     */
    protected $lineaFido;

    /**
     * @MongoDB\NotSaved
     */
    protected $importo;

    /**
     * @MongoDB\NotSaved
     */
    protected $lineaCredito;

    /**
     * @MongoDB\Field(type="string")
     */
    protected $recapitiTelefonici;

    /**
     * @MongoDB\Field(type="string")
     * @Assert\Email()
     * @Assert\NotBlank()
     */
    protected $email;

    /**
     * @MongoDB\Field(type="string")
     */
    protected $messaggio;

    /**
     * @MongoDB\Field(type="string")
     */
    protected $pagina;

    /**
     * @MongoDB\Date
     */
    protected $dataCreazione;

    /**
     * @MongoDB\Field(type="string")
     */
    protected $userAgent;

    /**
     * @MongoDB\Field(type="string")
     */
    protected $ip;

    /**
     * @MongoDB\Field(type="string")
     */
    protected $page;

    /**
     * @return mixed
     */
    public function getUserAgent()
    {
        return $this->userAgent;
    }

    /**
     * @param mixed $userAgent
     */
    public function setUserAgent($userAgent)
    {
        $this->userAgent = $userAgent;
    }

    /**
     * @return mixed
     */
    public function getDataCreazione()
    {
        return $this->dataCreazione;
    }

    /**
     * @param mixed $dataCreazione
     */
    public function setDataCreazione(\DateTime $dataCreazione)
    {
        $this->dataCreazione = $dataCreazione;
    }


    protected $privacy = true;

    /**
     * Get id.
     *
     * @return int $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * @param mixed $nome
     */
    public function setNome($nome)
    {
        $this->nome = $nome;
    }

    /**
     * @return mixed
     */
    public function getCognome()
    {
        return $this->cognome;
    }

    /**
     * @param mixed $cognome
     */
    public function setCognome($cognome)
    {
        $this->cognome = $cognome;
    }

    /**
     * @return mixed
     */
    public function getRagioneSociale()
    {
        return $this->ragioneSociale;
    }

    /**
     * @param mixed $ragioneSociale
     */
    public function setRagioneSociale($ragioneSociale)
    {
        $this->ragioneSociale = $ragioneSociale;
    }

    /**
     * @return mixed
     */
    public function getRecapitiTelefonici()
    {
        return $this->recapitiTelefonici;
    }

    /**
     * @param mixed $recapitiTelefonici
     */
    public function setRecapitiTelefonici($recapitiTelefonici)
    {
        $this->recapitiTelefonici = $recapitiTelefonici;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getMessaggio()
    {
        return $this->messaggio;
    }

    /**
     * @param mixed $messaggio
     */
    public function setMessaggio($messaggio)
    {
        $this->messaggio = $messaggio;
    }

    /**
     * @return mixed
     */
    public function getPagina()
    {
        return $this->pagina;
    }

    /**
     * @param mixed $pagina
     */
    public function setPagina($pagina)
    {
        $this->pagina = $pagina;
    }

    /**
     * @return mixed
     */
    public function getPrivacy()
    {
        return $this->privacy;
    }

    /**
     * @param mixed $privacy
     */
    public function setPrivacy($privacy)
    {
        $this->privacy = $privacy;
    }

    /**
     * @return mixed
     */
    public function getIp()
    {
        return $this->ip;
    }

    /**
     * @param mixed $ip
     */
    public function setIp($ip)
    {
        $this->ip = $ip;
    }

    /**
     * @param mixed $tipo
     */
    public function setTipo($tipo)
    {
        $this->tipo = $tipo;
    }

    public function getTipo()
    {
        return $this->tipo;
    }

    /**
     * @param mixed $nominativoReferente
     */
    public function setNominativoReferente($nominativoReferente)
    {
        $this->nominativoReferente = $nominativoReferente;
    }

    public function getNominativoReferente()
    {
        return $this->nominativoReferente;
    }

    /**
     * @param mixed $numeroConti
     */
    public function setNumeroConti($numeroConti)
    {
        $this->numeroConti = $numeroConti;
    }

    public function getNumeroConti()
    {
        return $this->numeroConti;
    }

    /**
     * @param mixed $lineaFido
     */
    public function setLineaFido($lineaFido)
    {
        $this->lineaFido = $lineaFido;
    }

    public function getLineaFido()
    {
        return $this->lineaFido;
    }

    /**
     * @param mixed $importo
     */
    public function setImporto($importo)
    {
        $this->importo = $importo;
    }

    public function getImporto()
    {
        return $this->importo;
    }

    /**
     * @param mixed $lineaCredito
     */
    public function setLineaCredito($lineaCredito)
    {
        $this->lineaCredito = $lineaCredito;
    }

    public function getLineaCredito()
    {
        return $this->lineaCredito;
    }

    public function getBodyContatto()
    {
        $body = '';
        $body .= 'Nome: '.$this->nome."\n\r";
        $body .= 'Cognome: '.$this->cognome."\n\r";
        $body .= 'Email: '.$this->email."\n\r";
        $body .= 'Recapiti telefonici: '.$this->recapitiTelefonici."\n\r";
        $body .= 'Messaggio: '.$this->messaggio."\n\r";
        $body .= 'Pagina di richiesta: '.$this->pagina."\n\r";
        $body .= 'User Agent: '.$this->userAgent."\n\r";
        $body .= 'IP: '.$this->ip."\n\r";

        return $body;
    }

    public function getBody()
    {
        $body = '';
        $body .= 'Nome: '.$this->nome."\n\r";
        $body .= 'Cognome: '.$this->cognome."\n\r";
        $body .= 'Ragione Sociale: '.$this->ragioneSociale."\n\r";
        $body .= 'Recapiti telefonici: '.$this->recapitiTelefonici."\n\r";
        $body .= 'Email: '.$this->email."\n\r";
        $body .= 'Messaggio: '.$this->messaggio."\n\r";
        $body .= 'Nominativo Referente: '.$this->nominativoReferente."\n\r";
        $body .= 'Numero Conti: '.$this->numeroConti."\n\r";
        $body .= 'Linea Fido: '.$this->lineaFido."\n\r";
        $body .= 'Importo: '.$this->importo."\n\r";
        $body .= 'Linea Di Credito: '.$this->lineaCredito."\n\r";
        $body .= 'Pagina: '.$this->pagina."\n\r";
        $body .= 'User Agent: '.$this->userAgent."\n\r";
        $body .= 'IP: '.$this->ip."\n\r";

        return $body;
    }

    public function getBodyCliente(){
        $body = '';
        $body .= 'La ringraziamo del suo ordine a breve sarà contattato da un nostro operatore. ';

        return $body;
    }
}