<?php

namespace AppBundle\EventListener;

use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Bundle\FrameworkBundle\Routing\Router;

class ErrorRedirect
{
    /**
     * Holds Symfony2 router
     *
     *@var Router
     */
    protected $router;
    protected $request;

    /**
     * @param Router
     */
    public function __construct(Router $router)
    {
        $this->router = $router;
    }

    /**
     * @param GetResponseForExceptionEvent $event
     */
    public function onKernelException(GetResponseForExceptionEvent $event)
    {
        $exception = $event->getException();

        if ($exception instanceof NotFoundHttpException) {
            if(preg_match('/[A-Z]/', $this->request->getPathInfo())) {
                $filteredUrl = substr($this->request->getPathInfo(), 0, strpos($this->request->getPathInfo(), "?"));
                if ($filteredUrl) {
                    $lowercaseUrl = strtolower($filteredUrl);
                } else {
                    $lowercaseUrl = strtolower($this->request->getPathInfo());
                }

                try {
                    if ($this->router->match($lowercaseUrl)) {
                        $response = new RedirectResponse($lowercaseUrl);
                        $event->setResponse($response);
                    }
                } catch (\RuntimeException $e){
                    /** Choose your router here */
                    $route = 'qm_usurabancaria_errore';

                    if ($route === $event->getRequest()->get('_route')) {
                        return;
                    }

                    $url = $this->router->generate($route);
                    $response = new RedirectResponse($url);
                    $event->setResponse($response);
                }

            } else {

                /** Choose your router here */
                $route = 'qm_usurabancaria_errore';

                if ($route === $event->getRequest()->get('_route')) {
                    return;
                }

                $url = $this->router->generate($route);
                $response = new RedirectResponse($url);
                $event->setResponse($response);
            }
        }
    }

    /**
     * @param RequestStack $requestStack
     */
    public function setRequest(RequestStack $requestStack){
        $this->request = $requestStack->getCurrentRequest();
    }
}
