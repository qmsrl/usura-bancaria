ExtractTextPlugin = require("extract-text-webpack-plugin");
//var path = require('path');

module.exports = {
    entry: {
        styleHome: "./app/Resources/public/twig/js/app.js",
        image: "./app/Resources/public/twig/js/image.js",
        styleTemplate: "./app/Resources/public/twig/js/template.js"
    },
    output: {
        path: "./web/assets",
        filename: "[name].js"
    },
    module: {
        loaders: [
            {
                test: /\.css$/, loader: ExtractTextPlugin.extract("style-loader", "css-loader")
            },
            {
                test: /\.jpe?g$|\.gif$|\.png$|\.svg$|\.woff$|\.woff2$|\.ttf$|\.wav$|\.ico$|\.eot$/,
                loader: "file?name=[name].[ext]"
            },
            {
                test: /[\\\/]bower_components[\\\/]modernizr[\\\/]modernizr\.js$/,
                loader: "imports?this=>window!exports?window.Modernizr"
            },
            {
                test: /\.(png|woff|woff2|eot|ttf|svg)(\?.+)$/,
                loader: 'url-loader'
            },
            /*{
                test: /\.modernizrrc.js$/,
                loader: "modernizr"
            },
            {
                test: /\.modernizrrc(\.json)?$/,
                loader: "modernizr!json"
            }*/
        ]
    },
    devtool: 'source-map',
    'node': {
        fs: 'empty',
        child_process: "empty"
    },
    plugins: [
        new ExtractTextPlugin("[name].css"),
    ],
    watch: false,
    /*resolve: {
        alias: {
            modernizr$: path.resolve(__dirname, ".modernizrrc")
        }
    }*/
};
