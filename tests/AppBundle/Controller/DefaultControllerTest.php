<?php

namespace Tests\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class DefaultControllerTest extends WebTestCase
{
    public function testIndex()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertContains('Calcola quanto puoi recuperare dalla Tua Banca! ', $crawler->filter('#page > section.intro-section.intro-section-26 > div.container.flex-cl > div > div.flex-col-md-6.flex-cl > div > h1')->text());
    }

    public function testErrorRoute(){
        $client = static::createClient();
        $crawler = $client->request('GET', '/ops');
        $this->assertEquals(302, $client->getResponse()->getStatusCode());
    }

    public function testFormContattiHome(){
        $client = static::createClient();
        $crawler = $client->request('GET' , '/');
        $form = $crawler->selectButton('AVANTI')->form([
            'contatto[tipo]' => 'persona' ,
            'contatto[nome]' => 'Maurizio',
            'contatto[cognome]' => 'Costanzo' ,
            'contatto[recapitiTelefonici]' => '3334455678',
            'contatto[email]' => 'mariatatata@gmeil.com',
            'contatto[numeroConti]' => '6',
            'contatto[lineaFido]' => 'si',
            'contatto[importo]' => '5000',
            'contatto[lineaCredito]' => 'Test',
            'contatto[privacy]' => '1'
        ]);
        $client->enableProfiler();
        $client->submit($form);
        $this->assertEquals(
          302,
          $client->getResponse()->getStatusCode()
        );

        $messages = $client->getProfile()->getCollector('swiftmailer')->getMessages();
        $crawler = $client->followRedirect();
        $this->assertEquals(
            200,
            $client->getResponse()->getStatusCode()
        );
        $message = $messages[0];

        $this->assertInstanceOf('Swift_Message', $message);
        $this->assertEquals('Calcolo Usura Bancaria', $message->getSubject());
        $this->assertEquals('comunicazioni@calcolousurabancaria.it', key($message->getFrom()));
        $this->assertEquals('comunicazioni@calcolousurabancaria.it', key($message->getTo()));
    }

    public function testFormContatti(){
        $client = static::createClient();
        $crawler = $client->request('GET' , '/contatti');
        $form = $crawler->selectButton('INVIA')->form([
            'form_contatti[nome]' => 'Maurizio',
            'form_contatti[cognome]' => 'Costanzo' ,
            'form_contatti[email]' => 'mariatatata@gmeil.com',
            'form_contatti[messaggio]' => '3334455678',
            'form_contatti[privacy]' => '1'
        ]);
        $client->enableProfiler();
        $client->submit($form);
        $this->assertEquals(
            302,
            $client->getResponse()->getStatusCode()
        );

        $messages = $client->getProfile()->getCollector('swiftmailer')->getMessages();
        $client->followRedirect();
        $this->assertEquals(
            200,
            $client->getResponse()->getStatusCode()
        );
        $message = $messages[0];

        $this->assertInstanceOf('Swift_Message', $message);
        $this->assertEquals('Calcolo Usura Bancaria', $message->getSubject());
        $this->assertEquals('comunicazioni@calcolousurabancaria.it', key($message->getFrom()));
        $this->assertEquals('comunicazioni@calcolousurabancaria.it', key($message->getTo()));
    }
}
