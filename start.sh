#!/bin/bash

sudo echo -n ""

SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ]; do
  DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
  SOURCE="$(readlink "$SOURCE")"
  [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE"
done
DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
cd $DIR

RED=$'\033[0;31m'
GREEN=$'\033[0;32m'
NC=$'\033[0m'

printf "Clearing Docker services"
cd docker
docker-compose down > ../log/general/docker.log 2>&1
cd ..
printf "%-13s[OK]%s\n" "$GREEN" "$NC"

printf "Checking codebase"

if [ "$(ls -A "src")" ]; then
  printf "%-20s[OK]%s\n" "$GREEN" "$NC"
#else
  #git clone git@bitbucket.org:qmsrl/gesto.git code 
fi

if [ ! "$(ls -A "src")" ]; then
  printf "%-20s[fail]%s\n" "$RED" "$NC"
  exit 1;
fi

printf "Clearing logs"
if [ ! -d "log" ]; then
  mkdir -m 777 log
  mkdir -m 777 log/usura-bancaria
else
  sudo rm -rf log/*/*.log*
fi

printf "%-24s[OK]%s\n" "$GREEN" "$NC"

printf "Starting Docker services"
export USURABANCARIA_DIR=$(pwd)
export COMPOSER_BINARY=/usr/local/bin/composer
export COMPOSER_HOME=/usr/local/composer


cd docker
if [ -f "docker-compose.yml" ]; then
  rm docker-compose.yml
fi

if [ -f "nginx/Dockerfile" ]; then
  rm nginx/Dockerfile
fi

if [ -f "php/Dockerfile" ]; then
  rm php/Dockerfile
fi

envsubst <"template.yml"> "docker-compose.yml"
envsubst <"nginx/Dockerfile.template"> "nginx/Dockerfile"
envsubst <"php/Dockerfile.template"> "php/Dockerfile"

docker-compose up -d >> ../log/general/docker.log 2>&1
cd ..

USURABANCARIA_NGINX="$(docker ps | grep usurabancaria_nginx)"
USURABANCARIA_PHP="$(docker ps  | grep usurabancaria_php)"

if [[ -z "${USURABANCARIA_NGINX// }" ]] || [[ -z "${USURABANCARIA_PHP// }" ]] ; then
  printf "%-13s[fail]%s\n" "$RED" "$NC"
  exit 1
fi
printf "%-13s[OK]%s\n" "$GREEN" "$NC"

printf "Initialize Symfony app"
  cd docker
  docker-compose exec php composer install >> ../log/general/docker.log 2>&1
  docker-compose exec php npm install >> ../log/general/docker.log 2>&1
  docker-compose exec php webpack >> ../log/general/docker.log 2>&1
  cd ..


cd docker
docker-compose run exec php sh /start.sh >> ../log/general/docker.log 2>&1
cd ..
printf "%-15s[OK]%s\n" "$GREEN" "$NC"
sudo echo -n ""
printf "Setting permissions"
sudo chmod -R 777 vendor
sudo chmod -R 777 node_modules
sudo chmod -R 777 bin
sudo chmod -R 777 var
printf "%-18s[OK]%s\n" "$GREEN" "$NC"
