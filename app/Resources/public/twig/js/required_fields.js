function checkFields(e) {

    var first = true;

    $(".req").each(function () {
        var field = {};

        var emailRegex = /^(\s*[0-9a-zA-Z]([-.\w]*[0-9a-zA-Z+-])*@([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)+[a-zA-Z]{2,9}\s*)$/;

        $(this).is(':visible') ? field.visible = true : field.visible = false;

        if ($(this).find('textarea').length)
        {
            var textArea = $(this).find('textarea');
            if ($(textArea).hasClass('form-control'))
            {
                $(textArea).val() == '' ? field.message = 'Compila questo campo:' : field.compiled = true;
            }
        }

        if ($(this).find('input').length) {
            var inputType = $(this).find('input');

            if ($(inputType).is(':disabled')) {
                field.disabled = true;
            }

            if ($(this).attr('class').indexOf('email') >= 0) {
                field.type = 'email';
            }

            if ($(this).attr('id') == 'altriServizi') {
                !$(inputType).is(':checked') ? field.type = 'altriServizi' : field.compiled = true;
            } else {
                if ($(inputType).is(':radio')) {
                    !$(inputType).is(':checked') ? field.message = 'Seleziona una di queste opzioni:' : field.compiled = true;
                }

                if ($(inputType).is(':checkbox')) {
                    !$(inputType).is(':checked') ? field.message = 'Seleziona questa casella se intendi procedere:' : field.compiled = true;
                }

                if ($(inputType).is(':text')) {
                    $(inputType).val() == '' ? field.message = 'Compila questo campo:' : field.compiled = true;
                }

                if (field.type == 'email' && field.compiled == true && !emailRegex.test($(inputType).val())) {
                    field.message = 'Inserisci un indirizzo email valido:';
                    field.wrongEmail = true;
                }
            }
        } else {
            if ($(this).find('select').is(':disabled')) {
                field.disabled = true;
            }

            //$(this).find('select').val() == 0 || $(this).find('select').val() == '' ? field.message = "Seleziona un elemento nell'elenco:" : field.compiled = true;
        }

        $(this).closest("div[class*='form-group']").hasClass('alert-form') ? field.alert = true : field.alert = false;


        if (field.visible && (!field.compiled || (field.compiled && field.wrongEmail)) && !field.disabled) {
            if (first) {
                e.preventDefault();
                //$('html, body').animate({scrollTop: $(this).closest("div[class*='row']").offset().top}, 300);
                first = false;
            }

            if (field.type === 'altriServizi') {
                $('#noSelection').removeClass('hidden');
            } else {
                if (!field.alert) {
                    $(this).closest("div[class*='form-group']").addClass('alert-form');
                    $(this).closest("div[class*='form-group']").addClass('alert-danger-form');
                    $(this).closest("div[class*='form-group']").prepend("<span class='alert-message'>" + field.message + "</span>");
                }
            }
        }

    });
}

function removeAlerts(field) {
    $(field).find("div[class*='alert-form']").length ? $(field).find("div[class*='alert-form']").removeClass('alert-form') : $(field).closest("div[class*='alert-form']").removeClass('alert-form');

    $(field).find("div[class*='alert-danger-form']").length ? $(field).find("div[class*='alert-danger-form']").removeClass('alert-danger-form') : $(field).closest("div[class*='alert-danger-form']").removeClass('alert-danger-form');

    $(field).find("div[class*='form-group']").find('.alert-message').length ? $(field).find("div[class*='form-group']").find('.alert-message').remove() : $(field).closest("div[class*='form-group']").find('.alert-message').remove();

}

$(document).ready(function () {
    $("form").on('propertychange change keyup keypress input paste', function () {
        $(".req").each(function () {
            if ($(this).find('input').length) {
                var inputType = $(this).find('input');

                if ($(this).attr('id') == 'altriServizi') {
                    $(this).click(function () {
                        $(inputType).is(':checkbox') && $(inputType).is(':checked') ? $('#noSelection').addClass('hidden') : null;
                    });
                } else {
                    $(inputType).is(':radio') && $(inputType).is(':checked') ? removeAlerts(this) : null;
                    $(inputType).is(':checkbox') && $(inputType).is(':checked') ? removeAlerts(this) : null;
                    $(inputType).is(':text') && $(inputType).val() !== '' ? removeAlerts(this) : null;
                }

                //removeAlerts(this);
            }
            if ($(this).find('textarea').length)
            {
                var textArea = $(this).find('textarea');
                $(textArea).val() !== '' ? removeAlerts(this) : null;
            }

            if($(this).is('select')) {
                //alert($(this).attr('id') + '  ' + $(this).val());
                var valore = $(this).val();

                if ($(this).val() != '') {
                    removeAlerts(this);
                }
            }


        });
    });

    $('#bottone').click(function (e) {
        checkFields(e);
    });
    
    $("#bottone").click(function( event ) {

        var emailRegex = /^(\s*[0-9a-zA-Z]([-.\w]*[0-9a-zA-Z+-])*@([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)+[a-zA-Z]{2,9}\s*)$/;

        if ($('#contatto_tipo_0').is(':checked') && !$('#contatto_recapitiTelefonici').val() =="" && !$('#contatto_email').val() == "" && emailRegex.test($('#contatto_email').val()) && !$('#stepSecondo').is(':visible'))
        {
            event.preventDefault();
            $('#titoloStepPrimo').addClass('hidden');
            $('#titoloForm').addClass('hidden');
            $('#tipo').addClass('hidden');
            $('#nome').addClass('hidden');
            $('#cognome').addClass('hidden');
            $('#ragioneSociale').addClass('hidden');
            $('#nominativoReferente').addClass('hidden');
            $('#recapitiTelefonici').addClass('hidden');
            $('#email').addClass('hidden');
            $('#titoloStepSecondo').removeClass('hidden');
            $('#stepSecondo').removeClass('hidden');
            $('#importo').addClass('hidden');
            $('#bottoneIndietro').removeClass('hidden');
        }
        if ($('#contatto_tipo_1').is(':checked') && !$('#contatto_recapitiTelefonici').val() == "" && !$('#contatto_email').val() == "" &&emailRegex.test($('#contatto_email').val()) && !$('#stepSecondo').is(':visible'))
        {
            event.preventDefault();
            $('#titoloStepPrimo').addClass('hidden');
            $('#titoloForm').addClass('hidden');
            $('#tipo').addClass('hidden');
            $('#nome').addClass('hidden');
            $('#cognome').addClass('hidden');
            $('#ragioneSociale').addClass('hidden');
            $('#nominativoReferente').addClass('hidden');
            $('#recapitiTelefonici').addClass('hidden');
            $('#email').addClass('hidden');
            $('#titoloStepSecondo').removeClass('hidden');
            $('#stepSecondo').removeClass('hidden');
            $('#importo').addClass('hidden');
            $('#bottoneIndietro').removeClass('hidden');
        }

    });
});
