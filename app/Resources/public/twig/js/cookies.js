/**
 * Created by daniele on 15/12/16.
 */
//var url = Routing.generate('qm_usurabancaria_privacy');

$(document).ready(function(){
    $.cookieBar({
        message: "Il nostro sito web utilizza cookies di terzi parti. Continuando la navigazione, si accetta l'uso dei cookies che ci consente di fornire un servizio ottimale e contenuti personalizzati.   ",
        acceptButton: true,
        acceptText: 'CHIUDI',
        policyButton: true,
        policyText: 'Informativa',
        policyURL: '/privacy',
        fixed: true,
        bottom: true,
        append: true,
        acceptOnContinue: true,
        acceptAnyClick: true,
    });
});