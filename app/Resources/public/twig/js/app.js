/**
 * Created by daniele on 18/11/16.
 */
//console.log('app loaded');
//----------CSS------------
require("style!../css/animate.css");
require("style!../css/custom.css");
require("style!../css/helper.css");
require("style!../css/main.css");
require("style!../css/responsive.css");
require("style!../css/rgen-grids.css");
require("style!../css/jquery.coockiebar.css");
//-------JAVASCRIPT--------
//window.$ = window.jQuery = require('jquery');
//Modernizr = require('modernizr');
require('script!../js/respond.min.js');
require("script!../js/bundle.js");
require("script!../js/html5shiv.js");
require("script!../js/jquery-shortcut");
require("script!../js/jquery.viewportchecker.min.js");
require("script!../js/plugins.js");
require("script!../js/ie-matchmedia.js");
require("script!../lib/Magnific-Popup/jquery.magnific-popup.min.js");
require("script!../lib/Swiper/js/swiper.jquery.js");
require("script!../lib/Vide/jquery.vide.min.js");
require("script!../lib/owl-carousel/owl.carousel.min.js");
require("script!../lib/smartmenus/jquery.smartmenus.min.js");
require("script!../lib/social-feed/js/jquery.socialfeed.js");
require("script!../lib/social-feed/bower_components/codebird-js/codebird.js");
require("script!../lib/social-feed/bower_components/doT/doT.min.js");
require("script!../lib/social-feed/bower_components/moment/moment.min.js");
require("script!../lib/superfish/dist/js/hoverIntent.js");
require("script!../lib/superfish/dist/js/superfish.min.js");
require("script!../lib/sweetalert/sweetalert.min.js");
require("script!../js/form-homepage.js");
require("script!../js/jquery.cookiebar.js");
require("script!../js/cookies.js");
require("script!../js/required_fields.js");
require("script!../js/rgen.js");


