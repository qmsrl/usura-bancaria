/**
 * Created by daniele on 07/12/16.
 */
$(document).ready(function ()
{
     $('#tipo').find('input:radio').prop('checked',false);
     $('#nome').find('input:text').val('');
     $('#cognome').find('input:text').val('');
     $('#ragioneSociale').find('input:text').val('');
     $('#nominativoReferente').find('input:text').val('');
     $('#recapitiTelefonici').find('input:text').val('');
     $('#email').find('input:text').val('');
     $('#contatto_numeroConti').val('');
     $('#lineaFido').find('input:radio').prop('checked', false);
     $('#importo').find('input:text').val('');
     $('#contatto_lineaCredito').val('');

     $('#tipo').find('input:radio').change(function () {
         if ($('#contatto_tipo_0').is(':checked'))
         {
             $('#nome').removeClass('hidden');
             $('#cognome').removeClass('hidden');
             $('#recapitiTelefonici').removeClass('hidden');
             $('#email').removeClass('hidden');
         }
         else
         {
             $('#nome').addClass('hidden');
             //$('#nome').find('text').val('');
             $('#cognome').addClass('hidden');
         }
     });

    $('#tipo').find('input:radio').change(function () {
        if ($('#contatto_tipo_1').is(':checked'))
        {
            $('#ragioneSociale').removeClass('hidden');
            $('#nominativoReferente').removeClass('hidden');
            $('#recapitiTelefonici').removeClass('hidden');
            $('#email').removeClass('hidden');
        }
        else
        {
            $('#ragioneSociale').addClass('hidden');
            $('#nominativoReferente').addClass('hidden');
        }
    });



    /*$("#bottone").click(function( event ) {

        var emailRegex = /^(\s*[0-9a-zA-Z]([-.\w]*[0-9a-zA-Z+-])*@([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)+[a-zA-Z]{2,9}\s*)$/;

        if ($('#contatto_tipo_0').is(':checked') && !$('#contatto_recapitiTelefonici').val() =="" && !$('#contatto_email').val() == "" && emailRegex.test($('#contatto_email').val()) && !$('#stepSecondo').is(':visible'))
        {
                event.preventDefault();
                $('#titoloStepPrimo').addClass('hidden');
                $('#titoloForm').addClass('hidden');
                $('#tipo').addClass('hidden');
                $('#nome').addClass('hidden');
                $('#cognome').addClass('hidden');
                $('#ragioneSociale').addClass('hidden');
                $('#nominativoReferente').addClass('hidden');
                $('#recapitiTelefonici').addClass('hidden');
                $('#email').addClass('hidden');
                $('#titoloStepSecondo').removeClass('hidden');
                $('#stepSecondo').removeClass('hidden');
                $('#importo').addClass('hidden');
                $('#bottoneIndietro').removeClass('hidden');
        }
        if ($('#contatto_tipo_1').is(':checked') && !$('#contatto_recapitiTelefonici').val() == "" && !$('#contatto_email').val() == "" &&emailRegex.test($('#contatto_email').val()) && !$('#stepSecondo').is(':visible'))
        {
                event.preventDefault();
                $('#titoloStepPrimo').addClass('hidden');
                $('#titoloForm').addClass('hidden');
                $('#tipo').addClass('hidden');
                $('#nome').addClass('hidden');
                $('#cognome').addClass('hidden');
                $('#ragioneSociale').addClass('hidden');
                $('#nominativoReferente').addClass('hidden');
                $('#recapitiTelefonici').addClass('hidden');
                $('#email').addClass('hidden');
                $('#titoloStepSecondo').removeClass('hidden');
                $('#stepSecondo').removeClass('hidden');
                $('#importo').addClass('hidden');
                $('#bottoneIndietro').removeClass('hidden');
        }

    });*/

    $("#bottoneIndietro").click(function( event ) {
        if ($('#contatto_tipo_0').is(':checked'))
        {
            event.preventDefault();
            $('#titoloStepSecondo').addClass('hidden');
            $('#contatto_numeroConti').val('');
            $('#lineaFido').find('input:radio').prop('checked', false);
            $('#importo').find('input:text').val('');
            $('#contatto_lineaCredito').val('');
            $('#stepSecondo').addClass('hidden');
            $('#bottoneIndietro').addClass('hidden');
            $('#titoloStepPrimo').removeClass('hidden');
            $('#titoloForm').removeClass('hidden');
            $('#tipo').removeClass('hidden');
            $('#nome').removeClass('hidden');
            $('#cognome').removeClass('hidden');
            $('#recapitiTelefonici').removeClass('hidden');
            $('#email').removeClass('hidden');
        }

        if ($('#contatto_tipo_1').is(':checked'))
        {
            event.preventDefault();
            $('#titoloStepSecondo').addClass('hidden');
            $('#stepSecondo').addClass('hidden');
            $('#contatto_numeroConti').val('');
            $('#lineaFido').find('input:radio').prop('checked', false);
            $('#importo').find('input:text').val('');
            $('#contatto_lineaCredito').val('');
            $('#bottoneIndietro').addClass('hidden');
            $('#titoloStepPrimo').removeClass('hidden');
            $('#titoloForm').removeClass('hidden');
            $('#tipo').removeClass('hidden');
            $('#ragioneSociale').removeClass('hidden');
            $('#nominativoReferente').removeClass('hidden');
            $('#recapitiTelefonici').removeClass('hidden');
            $('#email').removeClass('hidden');
        }
    });

    $('#lineaFido').find('input:radio').change(function () {
        if ($('#contatto_lineaFido_0').is(':checked'))
        {
            $('#importo').removeClass('hidden');
        }
        if ($('#contatto_lineaFido_1').is(':checked'))
        {
            $('#importo').addClass('hidden');
        }
    });


});